/*___Generated_by_IDEA___*/

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/me/androidshit/jtalk/src/net/ustyugov/jtalk/service/ConnectionServiceCall.aidl
 */
package net.ustyugov.jtalk.service;
public interface ConnectionServiceCall extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.ustyugov.jtalk.service.ConnectionServiceCall
{
private static final java.lang.String DESCRIPTOR = "net.ustyugov.jtalk.service.ConnectionServiceCall";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.ustyugov.jtalk.service.ConnectionServiceCall interface,
 * generating a proxy if needed.
 */
public static net.ustyugov.jtalk.service.ConnectionServiceCall asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.ustyugov.jtalk.service.ConnectionServiceCall))) {
return ((net.ustyugov.jtalk.service.ConnectionServiceCall)iin);
}
return new net.ustyugov.jtalk.service.ConnectionServiceCall.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.ustyugov.jtalk.service.ConnectionServiceCall
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
}
}
}
